#ifndef TTTRFAPPLICATION_H
#define TTTRFAPPLICATION_H

#include "CustomTRF/TRFClass.h"
#include "CustomTRF/TtTRFreader.h"

#include "TLorentzVector.h"

class TtTRFapplication {
public:
  TtTRFapplication(int ntags=3,std::string sample="data");
  ~TtTRFapplication(){};
  
//   void AddJet(int idx,float pt,float eta,float phi,float e,float weight,int istagged=-1);
  void AddJet(int idx,float pt,float eta,float phi,float e,int istagged=-1);
  void Reset();
  void Evaluate();
  float GetTRFweight();
  bool GetJetIsTagged(int idx);
  int GetNBTags();
  
private:
  bool m_debug;
  
  TtTRFreader *m_TRFreader0;
  TtTRFreader *m_TRFreader1;
//   std::vector<TtTRFreader*> m_systTRFreader0;
//   std::vector<TtTRFreader*> m_systTRFreader1;

  TRFClass *m_TRF;
  TRFClass *m_TRF0;
  TRFClass *m_TRF1;
  TRFClass *m_TRF2;
  
  std::vector<int> m_jet_index;
  std::vector<TLorentzVector> m_jet;
  std::vector<float> m_jet_weight;
  std::vector<int> m_jet_istagged;
  
  float m_weight;
  int m_ntags;
  int m_incl;
  
  std::vector<int> m_jet_istagged_ttTRF;
  int m_nBtags_ttTRF;
}; 

#endif
