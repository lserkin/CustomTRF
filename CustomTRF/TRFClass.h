#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include "TRandom3.h"

#ifndef TRFClass_h
#define TRFClass_h

class TRFClass : public TObject{
  public:
    TRFClass();
    virtual ~TRFClass();
    
    void SetWorkingPoints(std::vector<std::string> wp_names);
    void AddWorkingPoint(std::string name);
    
    void SetJetBtagEff(int jet_index,int wp_index,float eff);
    void SetJetBtagEff(int jet_index,std::string wp_name,float eff);
    void SetJetBtagEffs(int jet_index,std::vector<float> eff_vec);
    void SetJetsBtagEff(int wp_index,std::vector<float> eff_vec);
    void SetJetsBtagEff(std::string wp_name,std::vector<float> eff_vec);
    void SetJetsBtagEffs(std::vector<std::vector<float> > eff_vec);

    void SetBtagRequirement(int number_of_tags, bool inclusive, int wp_index=0);
    void SetBtagRequirement(int number_of_tags, bool inclusive, std::string wp_name);
    
    void Reset();
    void Evaluate(int EventNumber=0);

    float GetTRFweight();
    
    bool GetJetIsTagged(int jet_index,int wp_index=0);
    bool GetJetIsTagged(int jet_index,std::string wp_name);
    
    int GetNumBtagJets(int wp_index=0);
    int GetNumBtagJets(std::string wp_name);
    
    int GetSumBtagBins();
    float GetChosenPermutationProb();
    float GetPermutationProb(bool *perm);
    bool* GetChosenPermutation();

    // ...
    
    void SetDebug(bool debug=true);
    void PrintJets();
    void PrintChosenConfig();

  private:
    std::vector<std::string>         fWorkingPoint; // [wp_index]
    std::vector<std::vector<float> > fJetBtagEff;   // [jet_index][wp_index]
    std::vector<std::vector<int> >   fJetIsTag;     // [jet_index][wp_index] // FIXME:  std::vector<std::vector<bool> > creating problems...
    
    int fNumJets;
    int fNumReqBtag;
    bool fBtagReqIncl;
    int fBtagReqWPidx;
    float fTRFweight;
    
    bool fDebug;  
    TRandom3* myRand;
    
    // for chosen permutation
    std::vector<float> prob;
    int idx;
    bool **comb;
    std::vector<bool> tempTest; //nasty hack to keep track of the previous number of permutations
};

// Utility
int Combinations(const int num_jets,bool **comb,int min_btags,bool inclusive);

#endif
