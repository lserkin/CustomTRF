#ifndef TTTRFREADER_H
#define TTTRFREADER_H

#include <string>
#include "TH1F.h"

class TtTRFreader {
public:
  TtTRFreader(std::string fileName);
  ~TtTRFreader();
  
  float GetEff(float v1,float v2,float v3);
  
private:
  TH1F *h0; // tot
  TH1F *h1;
  TH1F *h2;
  TH1F *h3;
}; 

#endif
