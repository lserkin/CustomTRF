#include "CustomTRF/TtTRFreader.h"

#include <iostream>

#include "TDirectory.h"
#include "TFile.h"

TtTRFreader::TtTRFreader(std::string fileName){
  TDirectory *d0 = gDirectory;
  TFile *f = new TFile(fileName.c_str());
  if(f!=0x0){
    h0 = (TH1F*)f->Get("h_eff");
    h1 = (TH1F*)f->Get("h_eff_pt");
    h2 = (TH1F*)f->Get("h_eff_eta");
    h3 = (TH1F*)f->Get("h_eff_dRj");
  }
  else{
    std::cout << "CustomTRF::TtTRFreader: WARNING: File " << fileName << " cannot be openened... Skipping it." << std::endl; 
  }
//     d0->cd();
//     delete f;
}

TtTRFreader::~TtTRFreader(){}
  
float TtTRFreader::GetEff(float v1,float v2,float v3){
  if(h0==0x0 || h1==0x0 || h2==0x0 || h3==0x0) return 0;
  float btageff0 = h0->GetBinContent(1);
  float btageff  = h1->GetBinContent(h1->FindBin(v1));
  btageff *= h2->GetBinContent(h2->FindBin(v2)) / btageff0;
  btageff *= h3->GetBinContent(h3->FindBin(v3)) / btageff0;
  return btageff;
}
